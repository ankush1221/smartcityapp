import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ReferComponent } from './components/refer/refer.component';
import { HomeModule } from './home/home.module';

const routes: Routes = [
  {
    path: '',
    loadChildren: () =>
      import('./home/home.module').then((mod) => mod.HomeModule),
  },
  {
    path: 'refer/:id',
    component: ReferComponent,
  },
  {
    path: 'terms-and-conditions',
    loadChildren: () =>
      import('./terms/terms.module').then((mod) => mod.TermsModule),
  },
  {
    path: 'privacy-policy',
    loadChildren: () =>
      import('./privacy-policy/privacy-policy.module').then(
        (mod) => mod.PrivacyPolicyModule
      ),
  },
  {
    path: 'shop',
    loadChildren: () =>
      import('./shop/shop.module').then((mod) => mod.ShopModule),
  },
  {
    path: 'download',
    loadChildren: () =>
      import('./download/download.module').then((mod) => mod.DownloadModule),
  },
  // {
  //   path: 'poll/narwana',
  //   loadChildren: () =>
  //     import('./shop/shop.module').then((mod) => mod.ShopModule),
  // },
  // {
  //   path: 'poll/tohana',
  //   loadChildren: () =>
  //     import('./shop/shop.module').then((mod) => mod.ShopModule),
  // },
  {
    path: 'poll',
    loadChildren: () =>
      import('./poll/poll.module').then((mod) => mod.PollModule),
  },

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
