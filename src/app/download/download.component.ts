import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { DeviceDetectorService } from 'ngx-device-detector';
import { CommonService } from '../services/common.service';

@Component({
  selector: 'app-download',
  templateUrl: './download.component.html',
  styleUrls: ['./download.component.css']
})
export class DownloadComponent implements OnInit {

  constructor(public activatedroute: ActivatedRoute, 
    public router: Router, public commonService: CommonService,
    private deviceService: DeviceDetectorService) { 
    this.getQueryParams();
  }


  getQueryParams(){
    let utm_source = '';
    let utm_medium = '';
    let utm_term = '';
    let utm_campaign = '';
    let utm_content = '';
    let url = window.location.href;
    if(this.activatedroute.snapshot.queryParams && this.activatedroute.snapshot.queryParams.src){
      utm_source = this.activatedroute.snapshot.queryParams.src;
    }else {
      utm_source = 'web';
    }
    if(this.activatedroute.snapshot.queryParams && this.activatedroute.snapshot.queryParams.m){
      utm_medium = this.activatedroute.snapshot.queryParams.m;
    } else {
      utm_medium = 'anonymous';
    }
    if(this.activatedroute.snapshot.queryParams && this.activatedroute.snapshot.queryParams.t){
      utm_term = this.activatedroute.snapshot.queryParams.t;
    }
    if(this.activatedroute.snapshot.queryParams && this.activatedroute.snapshot.queryParams.c){
      utm_campaign = this.activatedroute.snapshot.queryParams.c;
    }
    if(this.activatedroute.snapshot.queryParams && this.activatedroute.snapshot.queryParams.p){
      utm_content = this.activatedroute.snapshot.queryParams.p;
    }
    let deviceDetails: any = this.deviceService.getDeviceInfo();
    console.log(deviceDetails, "device details");

    let request: any = {
      referenceTable : "TRACK_LINK_REQUEST",
      metaInfo:{
          deepLinkUrl: url,
          timeInMillis: new Date().getTime(),
          platform:"WEB"
      }
  };

  for(let key in deviceDetails){
    request.metaInfo[key] = deviceDetails[key];
  }


  let campaign = {
    utm_source : utm_source,
    utm_medium: utm_medium,
    utm_term: utm_term,
    utm_campaign: utm_campaign,
    utm_content: utm_content
  }
  
    this.metaInfoApiCall(request);
    this.openPlayStore(campaign);  
  }
  openPlayStore(request: any){
      window.open(
        `https://play.google.com/store/apps/details?id=com.food.narwanaFood.localFood&referrer=utm_source%3D${request.utm_source}%26utm_medium%3D${request.utm_medium}%26utm_campaign%3D${request.utm_campaign}%26utm_term%3D${request.utm_term}%26utm_content%3D${request.utm_content}`,
        '_self'
      );
  }

  metaInfoApiCall(request: any){
    this.commonService.setMetaDetails(request).subscribe((data: any) => {
      console.log(data, "data");
    }, (err: any) => {
      console.log(err, "err");
    })
  }

  ngOnInit(): void {
    // window.open(
    //   'https://play.google.com/store/apps/details?id=com.food.narwanaFood.localFood',
    //   '_self'
    // );
  }

}
