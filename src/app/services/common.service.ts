import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";

@Injectable({
    providedIn: 'root'
})

export class CommonService {
    baseUrl = 'https://api.mcityapp.com/mv/api/';

    constructor(private httpService: HttpClient){}

    setMetaDetails(request: any){
       return this.httpService.post(this.baseUrl + 'metaInfoTable/createOrUpdate', request);
    }
}