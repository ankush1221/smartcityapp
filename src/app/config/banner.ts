export const SlidesStoreData = [
  {
    // src: '/assets/images/sl/sl-banner1.jpg',
    src: '/assets/images/cityApp/banner1.png',
    title: 'CITY APP',
    alt: 'none',
    id: '1',
  },
  {
    // src: 'https://images.unsplash.com/photo-1538083024336-555cf8943ddc?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=66b476a51b19889e13182c0e4827af18&auto=format&fit=crop&w=1950&q=80',
    src: '/assets/images/cityApp/banner2.png',
    title: 'CITY APP',
    alt: 'none',
    id: '2',
  },
  {
    src: '/assets/images/cityApp/banner3.png',
    title: 'CITY APP',
    alt: 'none',
    id: '3',
  },
];

export class SlidesStore {
  id!: string;
  src!: string;
  alt!: string;
  title!: string;
}
