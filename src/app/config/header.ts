export const MenuLinks = [
  {
    name: 'Home',
    url: '/home',
    imageUrl: 'fa fa-home'
  },
  {
    name: 'About Us',
    url: '#about-us',
    imageUrl: 'fa fa-quote-left'
  },
  {
    name: 'Contact',
    url: '#contact',
    imageUrl: 'fa fa-map-marker'
  }
]

export const Logo = {
  name: "",
  // imageUrl: "fa fa-codepen"
  imageUrl: 'assets/images/cityApp/logo.svg'
}

export class MenuLink {
  name?: string;
  url?: string;
  imageUrl!: string;
}
export class NavLogo {
  name?: string;
  imageUrl!: string;
  navLink?: string;
}


