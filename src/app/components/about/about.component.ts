import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.css'],
})
export class AboutComponent implements OnInit {
  aboutUs: any;
  constructor() {
    this.aboutUs = {
      imageUrl: 'assets/images/cityApp/1.jpg',
      content: `
      The mission of this app is to make small cities smart and digital. CityApp offers a variety of services within a city so that it can be a single-stop solution for all the requirements within a city. Its main aim is to create a hyperlocal market and community where users can opt for various types of services directly via CityApp. Some of the offerings of the CityApp are:
      1. Free home delivery of food & related items
      2. Groceries, Fruits, and Vegetable at wholesale rates
      3. Services - Taxi booking, Plumber, Carpenter, Electrician, etc.
      4. Business advertisements and lead generation
      5. Business digital marketing

      Any type of business can open an online eCommerce store on CityApp and start generating orders/leads on CityApp intercity or intracity
      `,
    };
  }

  ngOnInit(): void {}
}
