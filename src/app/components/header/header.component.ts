import { Component, OnInit } from '@angular/core';
import {MenuLinks, MenuLink, NavLogo, Logo} from 'src/app/config/header';
@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  menuLinks = new Array<MenuLink>();
  logo = new NavLogo();
  constructor() { }

  ngOnInit(): void {
    this.menuLinks = MenuLinks;
    this.logo = Logo;
  }



}
