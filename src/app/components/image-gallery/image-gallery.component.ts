import { Component, OnInit } from '@angular/core';
import { OwlOptions } from 'ngx-owl-carousel-o';

@Component({
  selector: 'app-image-gallery',
  templateUrl: './image-gallery.component.html',
  styleUrls: ['./image-gallery.component.css']
})
export class ImageGalleryComponent implements OnInit {

  slidesStores = [
    {
      src: '/assets/images/cityApp/c1.png'
    },
    {
      src: '/assets/images/cityApp/c2.png'
    },
    {
      src: '/assets/images/cityApp/c3.png'
    },
    {
      src: '/assets/images/cityApp/c5.png'
    },
    {
      src: '/assets/images/cityApp/c6.png'
    },
    {
      src: '/assets/images/cityApp/c7.png'
    }
  ]
  constructor() { }

  ngOnInit(): void {
  }

  customOptions: OwlOptions = {
    loop: true,
    margin: 20,
    autoplay: true,
    dots: false,
    navText: ['<i class="fa fa-arrow-left"></i>', '<i class=" fa fa-arrow-right"></i>'],
    responsive: {
      0: {
       items: 1
     },
      480: {
       items: 1
     },
      940: {
       items: 4
     }
    },
   nav: true
  }

}
