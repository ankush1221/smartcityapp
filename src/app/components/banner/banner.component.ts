import { Component, OnInit } from '@angular/core';
import { OwlOptions } from 'ngx-owl-carousel-o';
import { SlidesStore, SlidesStoreData } from 'src/app/config/banner';
@Component({
  selector: 'app-banner',
  templateUrl: './banner.component.html',
  styleUrls: ['./banner.component.css'],
})
export class BannerComponent implements OnInit {
  slidesStores = new Array<SlidesStore>();
  constructor() {}

  ngOnInit(): void {
    this.slidesStores = SlidesStoreData
  }
  customOptions: OwlOptions = {
    loop: true,
    margin: 0,
    autoplay:true,
    animateOut: 'fadeOut',
    animateIn: 'fadeIn',
    navText: ['<i class="fa fa-arrow-left"></i>', '<i class=" fa fa-arrow-right"></i>'],
    responsive: {
      0: {
       items: 1
     },
      480: {
       items: 1
     },
      940: {
       items: 1
     }
    },
   nav: true
  }
}

