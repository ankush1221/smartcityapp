import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { DeviceDetectorService } from 'ngx-device-detector';
import { CommonService } from 'src/app/services/common.service';

@Component({
  selector: 'app-refer',
  templateUrl: './refer.component.html',
  styleUrls: ['./refer.component.css'],
})
export class ReferComponent implements OnInit {
  constructor(public activatedroute: ActivatedRoute, public router: Router,
    private deviceService: DeviceDetectorService,
    private commonService: CommonService) {}

  ngOnInit() {
    this.getQueryParams();
    if (this.activatedroute.snapshot.params.id) {
      window.open(
        `https://play.google.com/store/apps/details?id=com.food.narwanaFood.localFood&referrer=utm_source%3Dcityapp%26utm_medium%3Drefer%26utm_campaign%3D${this.activatedroute.snapshot.params.id}`,
        '_self'
      );
    }
  }

  getQueryParams(){
    let url = window.location.href;
    let deviceDetails: any = this.deviceService.getDeviceInfo();
    let request: any = {
      referenceTable : "TRACK_LINK_REQUEST",
      metaInfo:{
          deepLinkUrl: url,
          timeInMillis: new Date().getTime(),
          platform:"WEB"
      }
  };

  for(let key in deviceDetails){
    request.metaInfo[key] = deviceDetails[key];
  }
    this.metaInfoApiCall(request); 
  }

  metaInfoApiCall(request: any){
    this.commonService.setMetaDetails(request).subscribe((data: any) => {
      console.log(data, "data");
    }, (err: any) => {
      console.log(err, "err");
    })
  }



}
