import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css'],
})
export class FooterComponent implements OnInit {
  mobileNumber = '+91-9654854895';
  email = 'contactus@mcityapp.com';
  location = {
    name: 'Birbal Nagar, Narwana',
  };
  facebookUrl = 'https://www.facebook.com/cityAppOfficial';
  playStoreLink =
    'https://play.google.com/store/apps/details?id=com.food.narwanaFood.localFood';
  constructor() {}

  ngOnInit(): void {}
}
