import { Component, OnInit } from '@angular/core';
import { OwlOptions } from 'ngx-owl-carousel-o';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css']
})
export class ProductComponent implements OnInit {
  // slidesStores = [
  //   {
  //     src: '/assets/images/sl/p1.jpeg'
  //   },
  //   {
  //     src: '/assets/images/sl/p2.jpeg'
  //   },
  //   {
  //     src: '/assets/images/sl/p3.jpeg'
  //   },
  //   {
  //     src: '/assets/images/sl/p4.jpeg'
  //   }

  // ]

  slidesStores = [
    {
      src: '/assets/images/cityApp/v1.png'
    },
    {
      src: '/assets/images/cityApp/v2.png'
    },
    {
      src: '/assets/images/cityApp/v3.png'
    },
    {
      src: '/assets/images/cityApp/v4.png'
    },
    {
      src: '/assets/images/cityApp/v5.png'
    },
    {
      src: '/assets/images/cityApp/v6.png'
    },
    {
      src: '/assets/images/cityApp/v7.png'
    },
    {
      src: '/assets/images/cityApp/v8.png'
    },
    {
      src: '/assets/images/cityApp/v9.png'
    },
    {
      src: '/assets/images/cityApp/v10.png'
    },
    {
      src: '/assets/images/cityApp/v11.png'
    }


  ]


  constructor() { }

  ngOnInit(): void {
  }

  customOptions: OwlOptions = {
    loop: true,
    margin: 20,
    autoplay: true,
    dots: false,
    navText: ['<i class="fa fa-arrow-left"></i>', '<i class=" fa fa-arrow-right"></i>'],
    responsive: {
      0: {
       items: 1
     },
      480: {
       items: 1
     },
      940: {
       items: 4
     }
    },
   nav: true
  }



}
