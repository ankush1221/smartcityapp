import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { NarwanaPollComponent } from './narwana-poll/narwana-poll.component';
import { PollComponent } from './poll.component';
import { TestComponent } from './test/test.component';
import { TohanaPollComponent } from './tohana-poll/tohana-poll.component';

const routes: Routes = [
  {
    path: '', component: PollComponent,
    children: [
      {
        path: 'narwana',
        component: NarwanaPollComponent
      },
      {
        path: 'tohana',
        component: TohanaPollComponent
      },
      {
        path: 'test',
        component: TestComponent
      },
   
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PollRoutingModule { }
