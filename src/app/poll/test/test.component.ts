import { Component, OnInit } from '@angular/core';
import { DeviceDetectorService } from "ngx-device-detector";

@Component({
  selector: 'app-test',
  templateUrl: './test.component.html',
  styleUrls: ['./test.component.css']
})
export class TestComponent implements OnInit {
  // name = "Angular " + VERSION.major;
  deviceInfo: any;
  isDesktopDevice!: boolean;
  isTablet!: boolean;
  isMobile!: boolean;

  constructor(private deviceService: DeviceDetectorService) {
    this.epicFunction();
  }
  ngOnInit(): void {
      
  }

  epicFunction() {
    this.deviceInfo = this.deviceService.getDeviceInfo();
    this.isMobile = this.deviceService.isMobile();
    this.isTablet = this.deviceService.isTablet();
    this.isDesktopDevice = this.deviceService.isDesktop();
  }



}
