import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { DeviceDetectorService } from "ngx-device-detector";

declare global {
  interface Window {
    grecaptcha: any;
    grecaptchaCallback: any;
    RequestFileSystem: any;
    TEMPORARY: any;
    webkitRequestFileSystem: any;
  }
}



@Component({
  selector: 'app-narwana-poll',
  templateUrl: './narwana-poll.component.html',
  styleUrls: ['./narwana-poll.component.css']
})
export class NarwanaPollComponent implements OnInit {

  isDesktop: boolean = false;
  isVerified: boolean = false;
  @ViewChild('recaptcha', {static: true }) recaptchaElement !: ElementRef;

  constructor(private deviceService: DeviceDetectorService) {
  }

  ngOnInit(): void {

    window.open(
      'https://play.google.com/store/apps/details?id=com.food.narwanaFood.localFood',
      '_self'
    );

    
    // var fs = window.RequestFileSystem || window.webkitRequestFileSystem;
    // if (!fs) {
    //   console.log("check failed?");
    // } else {
    //   fs(window.TEMPORARY,
    //      100,
    //      console.log.bind(console, "not in incognito mode"),
    //      console.log.bind(console, "incognito mode"));
    // }



    // this.isDesktop = this.deviceService.isDesktop();
    // if(this.isDesktop){
    //   this.addRecaptchaScript();
    // } else {
    //      window.open(
    //   'https://www.opinionstage.com/city-app/%E0%A4%A8%E0%A4%B0%E0%A4%B5%E0%A4%BE%E0%A4%A8%E0%A4%BE-%E0%A4%AA%E0%A5%8D%E0%A4%B0%E0%A4%A7%E0%A4%BE%E0%A4%A8-election-anonymous-opinion-poll-by-city-app',
    //   '_self'
    //   );
    // }
  }

    renderReCaptch() {
    window['grecaptcha'].render(this.recaptchaElement.nativeElement, {
      'sitekey' : '6Leql0cgAAAAALePXrZ2MkEBIVPqEE0OpZDgVCLF',
      'callback': (response: any) => {
          console.log(response);
          if(response){
            // this.isVerified = true;

            if(document.getElementById('iframe')){
              document.getElementById('iframe')!.innerHTML = `<iframe name="opinionstage-widget" src="https://www.opinionstage.com/api/v1/widgets/1013417/iframe" data-opinionstage-iframe="418a4940-1561-4042-baac-82d8e9ad691e" width="100%" height="800" scrolling="auto" style="border: none;" frameBorder="0" allow="fullscreen" webkitallowfullscreen="true" mozallowfullscreen="true" referrerpolicy="no-referrer-when-downgrade"></iframe>`;
            }
            document.getElementById('main-wrap')!.style.display = 'none';
          }
      }
    });
  }

  addRecaptchaScript() {
 
    window['grecaptchaCallback'] = () => {
      this.renderReCaptch();
    }
 
    (function(d, s, id, obj){
      var js: any, fjs: any = d.getElementsByTagName(s)[0];
      if (d.getElementById(id)) { obj.renderReCaptch(); return;}
      js = d.createElement(s); js.id = id;
      js.src = "https://www.google.com/recaptcha/api.js?onload=grecaptchaCallback&render=explicit";
      fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'recaptcha-jssdk', this));
 
  }
}
