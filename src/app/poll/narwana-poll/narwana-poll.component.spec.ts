import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NarwanaPollComponent } from './narwana-poll.component';

describe('NarwanaPollComponent', () => {
  let component: NarwanaPollComponent;
  let fixture: ComponentFixture<NarwanaPollComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NarwanaPollComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NarwanaPollComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
