import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PollRoutingModule } from './poll-routing.module';
import { PollComponent } from './poll.component';
import { NarwanaPollComponent } from './narwana-poll/narwana-poll.component';
import { TohanaPollComponent } from './tohana-poll/tohana-poll.component';
import { TestComponent } from './test/test.component';


@NgModule({
  declarations: [
    PollComponent,
    NarwanaPollComponent,
    TohanaPollComponent,
    TestComponent
  ],
  imports: [
    CommonModule,
    PollRoutingModule
  ]
})
export class PollModule { }
