import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TohanaPollComponent } from './tohana-poll.component';

describe('TohanaPollComponent', () => {
  let component: TohanaPollComponent;
  let fixture: ComponentFixture<TohanaPollComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TohanaPollComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TohanaPollComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
