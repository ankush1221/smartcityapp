import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HomeRoutingModule } from './home-routing.module';
import { HomeComponent } from './home.component';
import { HeaderComponent } from '../components/header/header.component';
import { BannerComponent } from '../components/banner/banner.component';
import { AboutComponent } from '../components/about/about.component';
import { ImageGalleryComponent } from '../components/image-gallery/image-gallery.component';
import { FooterComponent } from '../components/footer/footer.component';
import { ProductComponent } from '../components/product/product.component';
import { CarouselModule } from 'ngx-owl-carousel-o';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    HomeComponent,
    HeaderComponent,
    BannerComponent,
    AboutComponent,
    ImageGalleryComponent,
    FooterComponent,
    ProductComponent,
  ],
  imports: [CommonModule, HomeRoutingModule, CarouselModule, FormsModule],
})
export class HomeModule {}
